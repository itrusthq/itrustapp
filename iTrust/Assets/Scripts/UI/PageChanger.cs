﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;


public class PageChanger : MonoBehaviour
{
    public GameObject HomePage;
    public GameObject Page1;
    public GameObject Page2;
    public GameObject Page3;
    public GameObject Page4;
    public int currentPage;

    //public List<Canvas> Pages = new List<Canvas>();
    public GameObject[] Pages;

    // Start is called before the first frame update
    void Start()
    {
        ClosePages();
        OpenHomePage();
        currentPage = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ClosePages()
    {
        //Page1.SetActive(false);
        //Page2.SetActive(false);
        //Page3.SetActive(false);
        //Page4.SetActive(false);
        for (int i=0; i< Pages.Length; i++)
        {
            Pages[i].SetActive(false);
        }
    }

    public void OpenHomePage()
    {
        ClosePages();
        currentPage = 0;
        HomePage.SetActive(true);
    }

    public void CurrentActivePage(int pageNum)
    {
        ClosePages();
        HomePage.SetActive(false);
        Pages[pageNum].SetActive(true);
    }

    public void NextPage()
    {
        if (currentPage < Pages.Length - 1)
        {
            currentPage += 1;
            CurrentActivePage(currentPage);
        }
    }
    
    public void PreviousPage()
    {
        if (currentPage >= 0)
        {
            currentPage -= 1;
            CurrentActivePage(currentPage);
        }
    }
}
