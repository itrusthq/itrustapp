﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class SelectBackground : MonoBehaviour
{
    public GameObject background1;
    public GameObject background2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeBackground1()
    {
        background1.SetActive(true);
        background2.SetActive(false);
    }

    public void ChangeBackground2()
    {
        background1.SetActive(false);
        background2.SetActive(true);
    }
}
