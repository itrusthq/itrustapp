﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class PlatformHandler : MonoBehaviour
{
    //public GameObject itemsPC;
    //public GameObject itemsAndroid;
    public GameObject[] listOfItemsPC;
    public GameObject[] listOfItemsAndroid;

    // Start is called before the first frame update
    void Start()
    {
        //if (listOfItemsPC == null)
            listOfItemsPC = GameObject.FindGameObjectsWithTag("PC");

        //if (listOfItemsAndroid == null)
            listOfItemsAndroid = GameObject.FindGameObjectsWithTag("Android");

        if (Application.platform == RuntimePlatform.WindowsPlayer)
        {
            foreach (GameObject itemsPC in listOfItemsPC)
            {
                itemsPC.SetActive(true);
            }
            foreach (GameObject itemsAndroid in listOfItemsAndroid)
            {
                itemsAndroid.SetActive(false);
            }
        }

        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WindowsEditor)
        {
            foreach (GameObject itemsPC in listOfItemsPC)
            {
                itemsPC.SetActive(false);
            }
            foreach (GameObject itemsAndroid in listOfItemsAndroid)
            {
                itemsAndroid.SetActive(true);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
