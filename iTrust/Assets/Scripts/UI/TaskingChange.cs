﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskingChange : MonoBehaviour
{
    public GameObject Scrollview1;
    public GameObject Scrollview2;

    // Start is called before the first frame update
    void Start()
    {
        Scrollview1.SetActive(true);
        Scrollview2.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenDetails()
    {
        Scrollview1.SetActive(false);
        Scrollview2.SetActive(true);
    }

    public void CloseDetails()
    {
        Scrollview1.SetActive(true);
        Scrollview2.SetActive(false);
    }
}
