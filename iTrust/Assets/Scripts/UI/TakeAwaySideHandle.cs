﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class TakeAwaySideHandle : MonoBehaviour
{
    public DanielLochner.Assets.SimpleSideMenu.SimpleSideMenu SideMenu;
    public Image SideHandle;
    public Image ThreeBars;
    public Text PageTitle;
    public RectTransform CanvasSize;
    public RectTransform HandleWidth;

    void HideHandle()
    {
        SetWidthOfBar();
        //SideHandle.enabled = false;
        ThreeBars.enabled = false;
        //PageTitle.enabled = false;
    }

    void ShowHandle()
    {
        SetWidthOfBar();
        //SideHandle.enabled = true;
        ThreeBars.enabled = true;
        //PageTitle.enabled = true;
    }

    public void SetWidthOfBar()
    {
        HandleWidth.sizeDelta = new Vector2(CanvasSize.sizeDelta.x, HandleWidth.sizeDelta.y);
        //HandleWidth.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, CanvasSize.sizeDelta.y);
    }

    void Awake()
    {
        SetWidthOfBar();
    }


    // Start is called before the first frame update
    void Start()
    {
        SetWidthOfBar();
    }

    // Update is called once per frame
    void Update()
    { 
        if (SideMenu.transform.position.x < 1)
        {
            ShowHandle();
        }

        else
        {
            HideHandle();
        }

    }
}
