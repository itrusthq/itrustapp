﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LinkHandler : MonoBehaviour
{

    public string linkToOpen;

    public Text HyperLinkInText;
    string LinkFromText;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TextLinkOpener()
    {
        LinkFromText = HyperLinkInText.text;
        Application.OpenURL(LinkFromText);
    }


    public void LinkOpener()
    {
        Application.OpenURL(linkToOpen);
    }
}
