﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;


public class CanvasChanger : MonoBehaviour
{
    //public Canvas StartPage;

    public Canvas LoginPage;
    public Canvas SideMenuPage;
    public Canvas MyProfilePage;
    public Canvas LogBookPage;
    public Canvas ERACPage;
    public Canvas TaskingsPage;
    public Canvas PanicPage;
    public List<Canvas> activeCanvases = new List<Canvas>();

    public GameObject background1;
    public GameObject background2;
    public Text Title;

    public PageChanger pageChanger;
    //int positionToAdd = 0;

    #region Personal Functions

    public void ClearCanvases()
    {
        //StartPage.enabled = false;
        LoginPage.enabled = false;
        MyProfilePage.enabled = false;
        LogBookPage.enabled = false;
        ERACPage.enabled = false;
        TaskingsPage.enabled = false;
        PanicPage.enabled = false;
    }

    public void ClearOverlay()
    {
        SideMenuPage.enabled = false;
    }

    public void ClearObjects()
    {
        ClearCanvases();
        ClearOverlay();
    }

    public void AddingCanvasToList(Canvas currentCanvas)
    {
        activeCanvases.Add(currentCanvas);
    }

    //public void StartPageOn()
    //{
    //    ClearObjects();
    //    StartPage.enabled = true;
    //    AddingCanvasToList(StartPage);
    //}

    public void LoginPageOn()
    {
        ClearObjects();
        LoginPage.enabled = true;
        ChangeBackground1();
        //AddingCanvasToList(LoginPage);
    }

    public void SideMenuPageOn()
    {
        //ClearCanvases();
        SideMenuPage.enabled = true;
    }
    public void MyProfilePageOn()
    {
        ClearCanvases();
        MyProfilePage.enabled = true;
        ChangeBackground2();
        ChangeTitle("My Profile");
        AddingCanvasToList(MyProfilePage);
    }

    public void LogBookPageOn()
    {
        ClearCanvases();
        LogBookPage.enabled = true;
        ChangeTitle("Log Book");
        AddingCanvasToList(LogBookPage);
    }
    public void ERACPageOn()
    {
        ClearCanvases();
        ERACPage.enabled = true;
        pageChanger.OpenHomePage();
        ChangeTitle("eMT-RAC");
        AddingCanvasToList(ERACPage);
    }
    public void TaskingsPageOn()
    {
        ClearCanvases();
        TaskingsPage.enabled = true;
        ChangeTitle("My Taskings");
        AddingCanvasToList(TaskingsPage);
    }
    public void PanicPageOn()
    {
        ClearCanvases();
        PanicPage.enabled = true;
        ChangeTitle("PANIC");
        AddingCanvasToList(PanicPage);
    }

    void ReturnToPreviousCanvas()
    {
        if (activeCanvases.Count > 1)
        {
            ClearCanvases();

            //if (activeCanvases[activeCanvases.Count - 2] == ScanPage)
            //{
            //    activeCanvases.Remove(activeCanvases[activeCanvases.Count - 1]);
            //    activeCanvases.Remove(activeCanvases[activeCanvases.Count - 2]);
            //    activeCanvases[activeCanvases.Count - 2].enabled = true;
            //}

            //else if (activeCanvases.Count > 4)
            //{

            //    if (activeCanvases[activeCanvases.Count - 2] == activeCanvases[activeCanvases.Count - 3])
            //    {
            //        activeCanvases[activeCanvases.Count - 4].enabled = true;
            //        activeCanvases.Remove(activeCanvases[activeCanvases.Count - 1]);
            //        activeCanvases.Remove(activeCanvases[activeCanvases.Count - 2]);
            //        activeCanvases.Remove(activeCanvases[activeCanvases.Count - 3]);
            //    }
            //}

            //else
            {
                activeCanvases.Remove(activeCanvases[activeCanvases.Count - 1]);
                activeCanvases[activeCanvases.Count - 1].enabled = true;
            }
        }
    }

    public void ChangeBackground1()
    {
        background1.SetActive(true);
        background2.SetActive(false);
    }

    public void ChangeBackground2()
    {
        background1.SetActive(false);
        background2.SetActive(true);
    }

    //public void HideTitle()
    //{
    //    Title.enabled = false;
    //}

    public void ChangeTitle(string title)
    {
        Title.text = title;
        //Title.enabled = true;
    }



    #endregion 

    private void Awake()
    {
        ClearCanvases();
        Screen.fullScreen = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        //StartPageOn();
        LoginPageOn();
        //(HomePage);
    }

    // Update is called once per frame
    void Update()
    {

        //Debug.Log(activeCanvases.Count);
        //if (Application.platform == RuntimePlatform.Android)
        //{
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                // Insert Code Here (I.E. Load Scene, Etc)
                ReturnToPreviousCanvas();
                //Debug.Log(activeCanvases[activeCanvases.Count - 1]);
                //Application.Quit();
                return;

            }
        //Debug.Log(activeCanvases.Count);
        //}

    }
}
