﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneCall : MonoBehaviour
{
    //public GameObject List;

    public void CallOpsRoom()
    {
        Application.OpenURL("tel://67964444");
    }

    public void CallS1()
    {
        Application.OpenURL("tel://67964441");
    }

    public void CallS3()
    {
        Application.OpenURL("tel://67964443");
    }

    public void CallS4()
    {
        Application.OpenURL("tel://67964437");
    }

    public void CallTPTOps()
    {
        Application.OpenURL("tel://67964450");
    }

    public void CallCOOffice()
    {
        Application.OpenURL("tel://67964434");
    }

    public void CallGSOC()
    {
        Application.OpenURL("tel://1733");
    }

    public void CallUSO()
    {
        Application.OpenURL("tel://96640015");
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
